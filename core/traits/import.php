<?php
namespace core\traits;

trait import
{
    protected static $import;

    public static function import($type, $link)
    {
        switch($type) {
            case CMP_COMPONENTS:
                self::$import = APP_PATH_COMPONENTS;
                break;

            case CMP_MODULES:
                self::$import = APP_PATH_MODULES;
                break;

            case CMP_PLUGINS:
                self::$import = APP_PATH_PLUGINS;
                break;
        }

        require_once self::$import.$link.CMP_PHP;
    }
}
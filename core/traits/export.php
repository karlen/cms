<?php
namespace core\traits;

use core\components\component;

trait export
{
    protected static $export;

    public static function export($link, $type, $data = false)
    {
        switch(get_class()) {
            case CMP_CLASS_COMPONENT:
                self::$export = APP_PATH_COMPONENTS;
                break;

            case CMP_CLASS_MODULE:
                self::$export = APP_PATH_MODULES;
                break;

            case CMP_CLASS_PLUGIN:
                self::$export = APP_PATH_PLUGINS;
                break;
        }

        switch($type) {
            case CMP_SCRIPTS:
                component::getInstance()->setScripts(self::$export.$link);
                break;

            case CMP_STYLES:
                component::getInstance()->setStyles(self::$export.$link);
                break;

            case CMP_TEMPLATES:
                component::getInstance()->setTemplates(self::$export.$link);
                break;

            case CMP_COMPONENTS:
                component::getInstance()->setComponents(self::$export.$link, $data);
                break;

            case CMP_MODULES:
                component::getInstance()->setModules(self::$export.$link, $data);
                break;

            case CMP_PLUGINS:
                component::getInstance()->setplugins(self::$export.$link, $data);
                break;
        }
    }
}
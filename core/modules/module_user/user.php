<?php
namespace core\modules\module_user;

class user
{
    private static $instance;

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    public
        $user,
        $userId,
        $userLogin,
        $userToken,
        $userLogged,
        $usersTable;

    private function __construct()
    {
        if ($this->userLoggedIn()) {
            $this->user = $this->getUserInfo();
            $this->userId = $this->user['id'];
            $this->userLogin = $this->user['login'];
            $this->userToken = $this->user['token'];
            $this->userLogged = true;
        } else {
            $this->user = false;
            $this->userId = false;
            $this->userLogin = false;
            $this->userToken = false;
            $this->userLogged = false;
        }
    }

    public function getUserInfo($userId = false){
        if($userId) {
            $user = db::getInstance()->get_fields($this->usersTable, 'id = "' . $userId . '"');
        } else {
            $user = db::getInstance()->get_fields($this->usersTable, 'token = "' . $this->userToken . '"');
        }
        return $user;
    }

    public function userLoggedIn()
    {
        if ($this->userToken && $this->userToken != '') {
            $user = explode('|', hashPi::getInstance()->uncompresspi($this->userToken));
            $mail = $user[0];
            $password = $user[1];
            $userResult = db::getInstance()->get_table($this->usersTable, 'mail = "'.$mail.'"  AND password = "'.$password.'"  AND token = "'.$this->userToken.'"  ');
            if ($userResult) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function userLogin($fields)
    {
        $mail = $fields['mail'];
        $password = $fields['password'];
        $password = hashPi::getInstance()->compresspi($password);
        $user = db::getInstance()->get_fields($this->usersTable, 'mail = "'.$mail.'"  AND password = "'.$password.'"');
        if ($user) {
            $token = hashPi::getInstance()->compresspi($mail . '|' . $password . '|' . time());
            $_SESSION['USER'] = $token;
            db::getInstance()->update($this->usersTable, array('token' => $token), $user['id']);
            return true;
        } else {
            return false;
        }
    }

    public function userExist($fields)
    {
        if(isset($fields['mail'])) {
            if(db::getInstance()->get_table($this->usersTable, 'mail = "'.$fields['mail'].'"')) {
                return true;
            } else {
                return false;
            }
        }

        if(isset($fields['phone'])) {
            if(db::getInstance()->get_table($this->usersTable, 'phone = "'.$fields['phone'].'"')) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    }

    public function registrationUser($fields)
    {
        $fields['password'] = hashPi::getInstance()->compresspi($fields['password']);

        if(!$this->userExist($fields['mail'])) {
            if (db::getInstance()->insert($this->usersTable, $fields)) {
                return true;
            } else {
                return config::getMessage('error_unknown_error');
            }
        } else {
            return config::getMessage('error_mail_exist');
        }
    }

    public function updateUser($fields,$id)
    {
        $fields['password'] = hashPi::getInstance()->compresspi($fields['password']);

        if (db::getInstance()->update($this->usersTable, $fields, $id)) {
            return true;
        } else {
            return config::getMessage('error_unknown_error');
        }
    }

    public function logOutUser()
    {
        $this->user['token'] = EMPTY_STRING;
        $userResult = db::getInstance()->update($this->usersTable,$this->user,$this->userId);
        if($userResult) {
            unset($_SESSION['USER']);
            return true;
        } else {
            return config::getMessage('error_unknown_error');
        }
    }
}
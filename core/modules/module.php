<?php
namespace core\modules;

use core\controllers\template;
use core\traits\export;
use core\traits\import;

class module
{
    use export;
    use import;

    public function printModule()
    {
        template::loadSmarty($this->name,$this->data,$this->template,$this->method);
    }
}
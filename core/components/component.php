<?php
namespace core\components;

use core\controllers\app;
use core\controllers\config;
use core\controllers\router;
use core\traits\export;
use core\traits\import;

class component
{
    use export;
    use import;

    public static $instance;

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    public function import($type, $name)
    {
        switch ($type)
        {
            case CMP_SCRIPTS:
                $this->setScripts($name);
                break;
            case CMP_STYLES:
                $this->setScripts($name);
                break;
        }
    }



    public function setScripts($name)
    {
        $script = $name.CMP_JS;

        if(array_key_exists($script, app::$scripts)) {
            array_push(app::$errors, config::getError("script-exist"));
        } else {
            array_push(app::$scripts, $script);
        }
    }

    public function setStyles($name)
    {
        $style = $name.CMP_CSS;

        if(array_key_exists($style, app::$styles)) {
            array_push(app::$errors, config::getError("style-exist"));
        } else {
            array_push(app::$styles, $style);
        }
    }

    public function setTemplates($name)
    {
        $style = $name.CMP_TPL;

        if(array_key_exists($style, app::$templates)) {
            array_push(app::$errors, config::getError("template-exist"));
        } else {
            array_push(app::$templates, $style);
        }
    }

    public function setComponents($name, $value)
    {
        if(array_key_exists($name, app::$components)) {
            array_push(app::$errors, config::getError("component-exist"));
        } else {
            array_push(app::$components, $value);
        }
    }

    public function setModules($name, $value)
    {
        if(array_key_exists($name, app::$modules)) {
            array_push(app::$errors, config::getError("module-exist"));
        } else {
            array_push(app::$modules, $value);
        }
    }

    public function setPlugins($name, $value)
    {
        if(array_key_exists($name, app::$plugins)) {
            array_push(app::$errors, config::getError("plugin-exist"));
        } else {
            array_push(app::$plugins, $value);
        }
    }

    public function printComponent()
    {
        if (app::$XHRState) {

        } else {
            router::printAjax();
        }
    }
}
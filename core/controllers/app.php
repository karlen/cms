<?php
namespace core\controllers;

require_once dirname(__FILE__)."/config.php";
require_once dirname(__FILE__)."/loader.php";

abstract class app
{
    public static
        $cache      = false,
        $XHRState   = false,
        $XHRMethod  = false,
        $component  = false,
        $templates  = Array(),
        $components = Array(),
        $modules    = Array(),
        $plugins    = Array(),
        $scripts    = Array(),
        $styles     = Array(),
        $configs    = Array(),
        $seoLink    = Array(),
        $errors     = Array();

    public static function appRun()
    {
        loader::appLoader();

        self::$configs   = config::getConfigs();
        self::$XHRState  = router::getXHRState();
        self::$XHRMethod = router::getXHRMethod();
        self::$seoLink   = router::appRouter(CMP_APP_URL);
        self::$component = router::appRouter(CMP_APP_COMPONENT);
    }
}
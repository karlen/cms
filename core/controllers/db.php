<?php
namespace core\controllers;

use core\traits\instance;

class db
{
    use instance
// ============================================================================ //
    public $join = '';
    public $joinColumns = '';
    public $select = '';
    public $where = '';
    public $groupBy = '';
    public $orderBy = '';
    public $limit = '1000';
    public $page = 1;
    public $perPage = 10;
    public $dbLink;
// ============================================================================ //
    protected static $db_host;
    protected static $db_base;
    protected static $db_user;
    protected static $db_pass;
// ============================================================================ //
    protected function __construct()
    {
        $this->dbLink = static::initConnection();
    }
// ============================================================================ //
    protected static function initConnection()
    {
        self::$db_host = config::getProtected('db_host');
        self::$db_base = config::getProtected('db_base');
        self::$db_user = config::getProtected('db_user');
        self::$db_pass = config::getProtected('db_pass');

        $dbLink = mysqli_connect(self::$db_host, self::$db_user, self::$db_pass, self::$db_base);

        if (mysqli_connect_errno()) {
            die('Cannot connect to MySQL server: ' . mysqli_connect_error());
        }
        mysqli_set_charset($dbLink, 'utf8');
        return $dbLink;
    }
// ============================================================================ //
    public static function reinitializedConnection()
    {

        $db = self::getInstance();

        if (!mysqli_ping($db->dbLink)) {
            if (!empty($db->dbLink)) {
                mysqli_close($db->dbLink);
            }
            $db->dbLink = self::initConnection();
        }
        return true;
    }
// ============================================================================ //
    public static function optimizeTables($tablesList = '')
    {
        $inDB = self::getInstance();

        if (is_array($tablesList)) {

            foreach ($tablesList as $tableName) {
                $inDB->query("OPTIMIZE TABLE $tableName");
                $inDB->query("ANALYZE TABLE $tableName");
            }
        } else if ($inDB->isTableExists('information_schema.tables')) {

            $tablesList = $inDB->get_table('information_schema.tables',"table_schema = '".DB_BASE,'table_name');

            if (!is_array($tablesList)) {
                return false;
            }

            foreach ($tablesList as $tableName) {
                $inDB->query("OPTIMIZE TABLE {$tableName['table_name']}");
                $inDB->query("ANALYZE TABLE {$tableName['table_name']}");
            }
        }

        if ($inDB->errno()) {
            return false;
        }
        return true;
    }
// ============================================================================ //
    public function isTableExists($table)
    {

        $this->query("SELECT*FROM ".$table." LIMIT 1");

        if ($this->errno()) {
            return false;
        }
        return true;
    }
// ============================================================================ //
    public function get_table($table, $where = '', $fields = '*')
    {
        $list = Array();
        $sql = "SELECT ".$fields." FROM ".$table."";

        if ($where) {
            $sql .= ' WHERE ' . $where;
        }
        $sql .= $this->orderBy;
        $sql .= ' LIMIT '.$this->limit;
        $result = $this->query($sql);

        if ($this->num_rows($result)) {
            while ($data = $this->fetch_assoc($result)) {
                $list[] = $data;
            }
            return $list;
        } else {
            return false;
        }
    }
// ============================================================================ //
    public function __destruct()
    {
        mysqli_close($this->dbLink);
    }

    public function resetConditions()
    {

        $this->where = '';
        $this->select = '';
        $this->join = '';
        $this->joinColumns = '';
        $this->groupBy = '';
        $this->orderBy = '';
        $this->limit = '1000';
        return $this;
    }
// ============================================================================ //
    public function addJoin($joinTable, $joinType = 'INNER JOIN', $joinParams)
    {
        $join = "".$joinType." ".$joinTable." ON ".$joinParams."";
        $this->join .= $join . "\n";
        return $this;
    }
    // ============================================================================ //
    public function join_query($table,$where = ''){
        $sql = "SELECT ".$this->joinColumns." FROM ".$table." ".$this->join." WHERE ".$where."";
        $result = $this->query($sql);
        if ($this->num_rows($result)) {
            $data = $this->fetch_assoc($result);
            return $data;
        } else {
            return false;
        }
    }
    // ============================================================================ //
    public function joinColumns($joinColumns)
    {
        $this->joinColumns .= $joinColumns . "\n";
        return $this;
    }
// ============================================================================ //
    public function addSelect($condition)
    {
        $this->select .= ', ' . $condition;
        return $this;
    }
// ============================================================================ //
    public function where($condition)
    {
        $this->where .= ' AND (' . $condition . ')' . "\n";
        return $this;
    }
// ============================================================================ //
    public function groupBy($field)
    {
        $this->groupBy = 'GROUP BY ' . $field;
        return $this;
    }
// ============================================================================ //
    public function orderBy($field, $direction = 'ASC')
    {
        $this->orderBy = 'ORDER BY ' . $field . ' ' . $direction;
        return $this;
    }
// ============================================================================ //
    public function limit($howMany)
    {
        return $this->limitIs(0, $howMany);
    }
// ============================================================================ //
    public function limitIs($from, $howMany = '')
    {
        $this->limit = (int)$from;
        if ($howMany) {
            $this->limit .= ', ' . $howMany;
        }
        return $this;
    }
// ============================================================================ //
    public function limitPage($page, $perPage)
    {
        $this->page = $page;
        $this->perPage = $perPage;
        return $this->limitIs(($page - 1) * $perPage, $perPage);
    }
// ============================================================================ //
    public function queryCache($sql, $clear = FALSE)
    {
        $filename = 'cache/' . $this->formatStr($sql) . '.sqcache';

        if (!file_exists($filename)) {
            $result = mysqli_query($this->dbLink, $sql);
            $arr = $this->fetch_all($result);
            $text = json_encode($arr, TRUE);
            $text = hashPi::getInstance()->compresspi($text);
            $fp = fopen($filename, "w");
            fwrite($fp, $text);
            fclose($fp);
            $result = $arr;
        } else {

            $time = time() - filectime($filename);
            $text = file_get_contents($filename);
            $text = hashPi::getInstance()->uncompresspi($text);
            $result = json_decode($text, TRUE);

            if ($time > 3600 || $clear) {
                unlink($filename);
            }
        }
        return $result;
    }
// ============================================================================ //
    public function formatStr($str)
    {
        $str = trim($str);
        $str = stripslashes($str);
        $str = htmlspecialchars($str);
        $str = str_replace(" ", "", $str);
        $str = $this->tohex($str);
        return $str;
    }
// ============================================================================ //
    public function tohex($string)
    {
        $hex = '';
        for ($i = 0; $i < strlen($string); $i++) {
            $hex .= dechex(ord($string[$i]));
        }
        return $hex;
    }
// ============================================================================ //
    public function fetch_all($result)
    {
        $array = array();
        if ($this->num_rows($result)) {
            while ($object = mysqli_fetch_assoc($result)) {
                $array[] = $object;
            }
        }
        return $array;
    }
// ============================================================================ //
    public function num_rows($result)
    {
        return mysqli_num_rows($result);
    }
// ============================================================================ //
    public function fetch_row($result)
    {
        return mysqli_fetch_row($result);
    }
// ============================================================================ //
    public function free_result($result)
    {
        return mysqli_free_result($result);
    }
// ============================================================================ //
    public function affected_rows()
    {
        return mysqli_affected_rows($this->dbLink);
    }
// ============================================================================ //
    public function rows_count($table, $where, $limit = 0)
    {

        $sql = "SELECT*FROM ".$table." WHERE ".$where;
        if ($limit) {
            $sql .= " LIMIT " . (int)$limit;
        }
        $result = $this->query($sql);
        return $this->num_rows($result);
    }
// ============================================================================ //
    public function query($sql)
    {

        if (empty($sql)) {
            return false;
        }

        $result = mysqli_query($this->dbLink, $sql);
        return $result;
    }
// ============================================================================ //
    public function get_field($table, $where='', $field='id')
    {
        $sql = "SELECT ".$field." as get_field FROM ".$table."";
        $sql .= ' WHERE '.$where;
        $sql .= $this->orderBy;
        $sql .= ' LIMIT 1';

        $result = $this->query($sql);

        if ($this->num_rows($result)) {
            $data = $this->fetch_assoc($result);
            return $data['get_field'];
        } else {
            return false;
        }
    }
// ============================================================================ //
    public function fetch_assoc($result)
    {
        return mysqli_fetch_assoc($result);
    }
// ============================================================================ //
    public function get_fields($table, $where, $fields = '*')
    {

        $sql = 'SELECT '.$fields.' FROM '.$table;
        $sql .= ' WHERE '.$where;
        $sql .= $this->orderBy;
        $sql .= ' LIMIT 1';
        $result = $this->query($sql);
        if ($this->num_rows($result)) {
            $data = $this->fetch_assoc($result);
            return $data;
        } else {
            return false;
        }
    }
// ============================================================================ //
    public function get_cells($table, $where, $cells)
    {

        $sql = 'SELECT '.$cells.' FROM '.$table;
        $sql .= 'WHERE '.$where;
        $sql .= $this->orderBy;
        $sql .= 'LIMIT '.$this->limit;

        $result = $this->query($sql);

        if ($this->num_rows($result)) {
            $data = $this->fetch_assoc($result);
            echo $data;
            return $data;
        } else {
            return false;
        }
    }
// ============================================================================ //
    public function error()
    {
        return mysqli_error($this->dbLink);
    }
// ============================================================================ //
    public function escape_string($value)
    {

        if (is_array($value)) {

            foreach ($value as $key => $string) {
                $value[$key] = $this->escape_string($string);
            }
            return $value;
        }
        return mysqli_real_escape_string($this->dbLink, stripcslashes($value));
    }
// ============================================================================ //
    public function isFieldExists($table, $field)
    {

        $sql = "SHOW COLUMNS FROM $table WHERE Field = '$field'";
        $result = $this->query($sql);

        if ($this->errno()) {
            return false;
        }
        return (bool)$this->num_rows($result);
    }
// ============================================================================ //
    public function errno()
    {
        return mysqli_errno($this->dbLink);
    }
// ============================================================================ //
    public function isFieldType($table, $field, $type)
    {

        $sql = "SHOW COLUMNS FROM $table WHERE Field = '$field' AND Type = '$type'";
        $result = $this->query($sql);

        if ($this->errno()) {
            return false;
        }
        return (bool)$this->num_rows($result);
    }
// ============================================================================ //
    /**
     * Добавляет массив записей в таблицу
     * ключи массива должны совпадать с полями в таблице
     */
    public function insert($table, $insert_array, $ignore = false)
    {

        // убираем из массива ненужные ячейки

        $insert_array = $this->removeTheMissingCell($table, $insert_array);
        $set = '';

        // формируем запрос на вставку в базу

        foreach ($insert_array as $field => $value) {
            $set .= "{$field} = '{$value}',";
        }

        // убираем последнюю запятую

        $set = rtrim($set, ',');
        $i = $ignore ? 'IGNORE' : '';
        $this->query("INSERT ".$i." INTO ".$table." SET ".$set."");

        if ($this->errno()) {
            return false;
        }
        return $this->get_last_id($table);
    }
// ============================================================================ //
    /**
     * Убирает из массива ячейки, которых нет в таблице назначения
     * используется при вставке/обновлении значений таблицы
     */
    public function removeTheMissingCell($table, $array)
    {

        $result = $this->query("SHOW COLUMNS FROM `".$table."`");
        $list = array();
        while ($data = $this->fetch_assoc($result)) {
            $list[$data['Field']] = '';
        }

        // убираем ненужные ячейки массива

        foreach ($array as $k => $v) {
            if (!isset($list[$k])) {
                unset($array[$k]);
            }
        }

        if (!$array || !is_array($array)) {
            return array();
        }
        return $array;
    }
// ============================================================================ //
    public function get_last_id($table = '')
    {

        if (!$table) {
            return (int)mysqli_insert_id($this->dbLink);
        }

        $result = $this->query("SELECT LAST_INSERT_ID() as lastID FROM ".$table." LIMIT 1");

        if ($this->num_rows($result)) {
            $data = $this->fetch_assoc($result);
            return $data['lastID'];
        } else {
            return 0;
        }
    }
// ============================================================================ //
    /**
     * Обновляет данные в таблице
     * ключи массива должны совпадать с полями в таблице
     */
    public function update($table, $update_array, $id)
    {

        if (isset($update_array['id'])) {
            unset($update_array['id']);
        }
        // id или where

        if (is_numeric($id)) {
            $where = "id = '{$id}' LIMIT 1";
        } else {
            $where = $id;
        }

        // убираем из массива ненужные ячейки

        $update_array = $this->removeTheMissingCell($table, $update_array);
        $set = '';

        // формируем запрос на вставку в базу

        foreach ($update_array as $field => $value) {
            $set .= "{$field} = '{$value}',";
        }

        // убираем последнюю запятую

        $set = rtrim($set, ',');
        $this->query("UPDATE ".$table." SET ".$set." WHERE ".$where."");

        if ($this->errno()) {
            return false;
        }
        return true;
    }
// ============================================================================ //
    public function delete($table, $where = '', $limit = 0)
    {

        $sql = "DELETE FROM ".$table." WHERE ".$where;

        if ($limit) {
            $sql .= " LIMIT {$limit}";
        }

        $this->query($sql);

        if ($this->errno()) {
            return false;
        }
        return true;
    }
// ============================================================================ //
    public function setFlags($table, $items, $flag, $value)
    {
        foreach ($items as $id) {
            $this->setFlag($table, $id, $flag, $value);
        }
        return $this;
    }

    public function setFlag($table, $id, $flag, $value)
    {
        $this->query("UPDATE ".$table." SET ".$flag." = '".$value."' WHERE id='".$id."'");
        return $this;
    }
}
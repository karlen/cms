<?php
namespace core\controllers;

use core\plugins\plugin_css\css;
use core\plugins\plugin_js\js;

class cache
{
    public static
        $stylesDir = CMP_CACHE_CSS_PATH,
        $scriptsDir = CMP_CACHE_JS_PATH,
        $schemesDir = CMP_CACHE_SCH_PATH,
        $templatesDir = CMP_CACHE_TMP_PATH;


    public static function setCaches()
    {
        app::$cache["component"] = app::$component;
        app::$cache["modules"]   = app::$modules;
        app::$cache["plugins"]   = app::$plugins;
        app::$cache["scripts"]   = app::$scripts;
        app::$cache["styles"]    = app::$styles;

        print_r(app::$cache);

        self::cacheSchema(app::$cache);
        self::cacheScripts(app::$scripts);
        self::cacheStyles(app::$styles);
    }

    public static function readCache()
    {


    }

    protected static function cacheScripts($scriptFiles, $fileName = false)
    {
        $fileDir = !$fileName ? app::$component : $fileName;

        $scriptName = self::$scriptsDir.$fileDir.CMP_JS;

        if (!file_exists($fileName)) {
            $Js = fopen($scriptName, "w") or die(config::getError('open-file'));
            fwrite($Js, js::getScripts($scriptFiles));
            fclose($Js);
        } else {
            if(app::$cache) {
                $Js = fopen($scriptName, "w+") or die(config::getError('overwrite-file'));
                fwrite($Js, js::getScripts($scriptFiles));
                fclose($Js);
            }
        }
    }

    protected static function cacheStyles($styleFiles, $fileName = false)
    {
        $fileDir = !$fileName ? app::$component : $fileName;

        $styleName = self::$stylesDir.$fileDir.CMP_CSS;

        if (!file_exists($styleName)) {
            $Css = fopen($styleName, "w") or die(config::getError('open-file'));
            fwrite($Css, css::getStyles($styleFiles));
            fclose($Css);
        } else {
            if(app::$cache) {
                $Css = fopen($styleName, "w+") or die(config::getError('overwrite-file'));
                fwrite($Css, css::getStyles($styleFiles));
                fclose($Css);
            }
        }
    }

    protected static function cacheSchema($schemaArray, $fileName = false)
    {
        $fileDir = !$fileName ? app::$component : $fileName;

        $schemaName = self::$schemesDir.$fileDir.CMP_JSON;

        if (!file_exists($schemaName)) {
            $schema = fopen($schemaName, "w") or die(config::getError('open-file'));
            fwrite($schema, json_encode($schemaArray, true));
            fclose($schema);
        } else {
            if(app::$cache) {
                $schema = fopen($schemaName, "w+") or die(config::getError('overwrite-file'));
                fwrite($schema, json_encode($schemaArray, true));
                fclose($schema);
            }
        }
    }
}
<?php
namespace core\controllers;

class router
{
    public static function formatURL($string = false)
    {
        $string = ($string) ? trim($_GET['page']) : trim($string);
        $string = stripslashes($string);
        $string = htmlspecialchars($string);
        $string = preg_replace("/[^a-zA-Z0-9-\/\s]/", "", $string);
        $string = explode('/',$string);
        return $string;
    }

    public static function appRouter($string)
    {
        switch ($string) {
            case CMP_APP_URL:
                $url = self::formatURL();
                break;
            case CMP_APP_COMPONENT:
                $url = self::formatURL();
                $url = ($url[0]) ? $url[0] : CMP_MAIN;
                break;
            case CMP_APP_PAGE:
                $url = self::formatURL();
                $url = ($url[1]) ? $url[0] : false;
                break;
            case CMP_APP_SUB_PAGE:
                $url = self::formatURL();
                $url = ($url[2]) ? $url[0] : false;
                break;
            default:
                $url = self::formatURL();
        }

        return $url;
    }

    public static function getXHRState() {
        return (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == CMP_XHR_TYPE);
    }

    public static function getXHRMethod() {
        return (self::getXHRState()) ? $_SERVER['REQUEST_METHOD'] : false;
    }

    protected static function not_found()
    {

    }

//    public static function printAjax()
//    {
//        $rule = app::$seoLink;
//        $controller = $rule[1];
//        $do = $rule[2];
//        $getInstance = GET_INSTANCE;
//
//        if(page::getInstance()->ajaxPost) {
//
//            if (isset($_POST['html']) && isset($_POST['css']) && isset($_POST['js'])) {
//
//                $this->cssMinFile = MIN_CSS_PATH . XHR . $controller . MIN_CSS;
//                $this->jsMinFile = MIN_JS_PATH . XHR . $controller . MIN_JS;
//
//                $response['css'] = $this->printXHRStyles($this->cssMinFile, $_POST['css']);
//                $response['js'] = $this->printXHRStyles($this->jsMinFile, $_POST['js']);
//
//                if (in_array($controller, $this->modules)) {
//                    $className = MODULE . $controller;
//                    $response['html'] = $className::$getInstance()->$do($_POST);
//                } else if (in_array($controller, $this->plugins)) {
//                    $className = PLUGIN . $controller;
//                    $response['html'] = $className::$getInstance()->$do($_POST);
//                }
//
//                echo json_encode($response, true);
//
//            } else {
//
//                if (in_array($controller, $this->modules)) {
//                    $className = MODULE . $controller;
//                    $className::$getInstance()->$do($_POST);
//                } else if (in_array($controller, $this->plugins)) {
//                    $className = PLUGIN . $controller;
//                    $className::$getInstance()->$do($_POST);
//                }
//
//            }
//        }
//
//        if(page::getInstance()->ajaxGet) {
//
//            if (in_array($controller, $this->modules)) {
//                $className = MODULE . $controller;
//                $className::$getInstance()->$do($_GET);
//            } else if (in_array($controller, $this->plugins)) {
//                $className = PLUGIN . $controller;
//                $className::$getInstance()->$do($_GET);
//            }
//
//        }
//    }
}
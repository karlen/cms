<?php
namespace core\controllers;

abstract class config
{
    static private $protected = array(); // For DB / passwords etc
    static public $public = array(); // For all public strings such as meta stuff for site
    static public $errors = array(); // For all public strings such as meta stuff for site

    public static function getProtected($key)
    {
        return isset(self::$protected[$key]) ? self::$protected[$key] : false;
    }

    public static function getPublic($key)
    {
        return isset(self::$public[$key]) ? self::$public[$key] : false;
    }

    public static function setProtected($key,$value)
    {
        self::$protected[$key] = $value;
    }

    public static function setPublic($key,$value)
    {
        self::$public[$key] = $value;
    }

    public function __get($key)
    {
        //$this->key // returns public->key
        return isset(self::$public[$key]) ? self::$public[$key] : false;
    }

    public function __isset($key)
    {
        return isset(self::$public[$key]);
    }

    public static function getError($name)
    {
        return self::$errors[$name];
    }

    public static function setErrors()
    {

    }

    public static function getConfigs()
    {
        self::setProtected('db_host', '185.28.20.9');
        self::setProtected('db_base', 'u887903156_avia');
        self::setProtected('db_user', 'u887903156_avia');
        self::setProtected('db_pass', '055878349');

        //Server and site configs
        define("CMP_PATH",             '/vagrant/web/Aviamayak/');
        define("CMP_PATH_CORE",        CMP_PATH.'core/');
        define("CMP_PATH_COMPONENTS",  CMP_PATH_CORE.'components/');
        define("CMP_PATH_MODULES",     CMP_PATH_CORE.'modules/');
        define("CMP_PATH_PLUGINS",     CMP_PATH_CORE.'plugins/');
        define("CMP_PATH_TRAITS",      CMP_PATH_CORE.'traits/');
        define("CMP_PATH_INCLUDES",    CMP_PATH_CORE.'includes/');

        //App cache configs
        define("CMP_CACHE_PATH",     'cache/');
        define("CMP_CACHE_JS_PATH",  'cache/js/');
        define("CMP_CACHE_CSS_PATH", 'cache/css/');
        define("CMP_CACHE_TMP_PATH", 'cache/templates/');
        define("CMP_CACHE_SCH_PATH", 'cache/schemes/');

        //App paths configs
        define("APP_PATH",                 CMP_PATH.'app/');
        define("APP_PATH_COMPONENTS",      APP_PATH.'components/');
        define("APP_PATH_MODULES",         APP_PATH.'modules/');
        define("APP_PATH_PLUGINS",         APP_PATH.'plugins/');
        define("APP_PATH_ASSETS",          APP_PATH.'assets/');
        define("APP_PATH_IMAGES",          APP_PATH.'images/');
        define("APP_PATH_MAIN_SCRIPTS",    APP_PATH_ASSETS.'js/');
        define("APP_PATH_MAIN_STYLES",     APP_PATH_ASSETS.'css/');
        define("APP_PATH_SCRIPTS",         'js/');
        define("APP_PATH_STYLES",          'css/');
        define("APP_PATH_TEMPLATES",       'templates/');

        //file configs
        define("CMP_PHP",   '.php');
        define("CMP_INI",   '.ini');
        define("CMP_CLASS", '.class'.CMP_PHP);
        define("CMP_TPL",   '.tpl');
        define("CMP_JSON",  '.json');
        define("CMP_TXT",   '.txt');
        define("CMP_HTML",  '.html');
        define("CMP_HTM",   '.htm');
        define("CMP_JS",    '.js');
        define("CMP_CSS",   '.css');

        //router configs
        define("CMP_MAIN",           'main');
        define("CMP_XHR_TYPE",       'xmlhttprequest');
        define("CMP_APP_URL",        'app-url');
        define("CMP_APP_COMPONENT",  'app-component');
        define("CMP_APP_PAGE",       'app-page');
        define("CMP_APP_SUB_PAGE",   'app-sub-page');

        //import and explode configs
        define("CMP_SCRIPTS",     'scripts');
        define("CMP_STYLES",      'styles');
        define("CMP_TEMPLATES",   'templates');
        define("CMP_COMPONENTS",  'components');
        define("CMP_MODULES",     'modules');
        define("CMP_PLUGINS",     'plugins');

        //CMP core classes
        define("CMP_CLASS_MODULE",     'core\modules\module');
        define("CMP_CLASS_PLUGIN",     'core\plugins\plugin');
        define("CMP_CLASS_COMPONENT",  'core\components\component');


//        //Language configs
//        define('LANGUAGE', 'RU');
//
//        //Core Configs
//        define("MAIN","main");
//        define("PREFIX","_");
//        define("EMPTY_STRING", '');
//        define("RESULT_TRUE", 'true');
//        define("RESULT_FALSE", 'false');
//        define("SERVICES","services");
//        define("ADMIN_IDENTIFIER","_admin");
//        define("GET_INSTANCE","getInstance");
//        define("CORE_CLASSES_PATH", PATH.'/core/classes/');
//        define("FILE_WRITE_ERROR", 'Unable to open file');
//        define("FILE_OVERWRITE_ERROR", 'Unable to overwrite file');
//
//        //Module configs
//        define("MODULE", 'module_');
//        define("MODULES_PATH", PATH.'/modules/');
//        define("MODULE_FUNC","printModule");
//        define("MODULE_REDACT_FUNC","redactModule");
//
//        //Plugin configs
//        define("PLUGIN", 'plugin_');
//        define("PLUGINS_PATH", PATH.'/plugins/');
//        define("GET_PLUGIN", "getPlugin");
//        define("GET_DATA_FUNC", "getData");
//        define("CREATE_FORM", "createForm");
//
//        //Component configs
//        define("COMPONENT", 'component_');
//        define("COMPONENTS_PATH", PATH.'/components/');
//
//        //PHP configs
//        define("PHP", '.php');
//        define("INDEX_PHP", '/index.php');
//        define("CLASS_PHP", '.class.php');
//        define("DATA_MYSQL", 'mysql');
//
//        //JSON configs
//        define("DATA_JSON", 'json');
//        define("JSON", '.json');
//        define("JSON_IDENTIFIER", "{");
//        define("SETTINGS_PATH", "/settings/");
//        define("INDEX_JSON", '/settings/index.json');
//        define("ADMIN_JSON", '/settings/admin.json');
//
//        //JS configs
//        define("_JS", '.js');
//        define("JS", '/js/*.js');
//        define("JS_FILES", '/js/');
//        define("JS_SEPARATOR", '');
//        define("XHR", 'XHR_');
//        define("INDEX_JS", '/js/index.js');
//        define("MIN_JS", '.min.js');
//        define("GLOB_JS", 'includes/assets/js/*.js');
//        define("MIN_JS_PATH", 'includes/assets/js/min/');
//
//        //CSS configs
//        define("_CSS", '.css');
//        define("CSS", '/css/*.css');
//        define("CSS_FILES", '/css/');
//        define("MIN_CSS", '.min.css');
//        define("CSS_SEPARATOR", '');
//        define("INDEX_CSS", '/css/index.css');
//        define("GLOB_CSS", 'includes/assets/css/*.css');
//        define("MIN_CSS_PATH", 'includes/assets/css/min/');
//
//        //TPL configs
//        define("TPL", '.tpl');
//        define("TEMPLATES", '/templates/');
//        define("INDEX_TPL", '/templates/index.tpl');
//        define("CACHE_DIR", PATH.'/includes/cache/');
//        define("SMARTY", 'Smarty');
//        define("SMARTY_INTERNAL", 'Smarty_Internal');
//        define("SMARTY_CLASS_PATH", PATH.'/includes/smarty/libs/');
//        define("SMARTY_PLUGINS_PATH", PATH.'/includes/smarty/libs/sysPlugins/');
//
//        //Tickets configs
//        define("ACTIVE_TICKETS",    'tickets-flight');
//        define("TICKETS_FLIGHT",    'tickets-flight');
//        define("TICKETS_CHARTER",   'tickets-charter');
//        define("TICKETS_HOTEL",     'tickets-hotel');
//        define("TICKETS_TOUR",      'tickets-tour');
//        define("TICKETS_TRAIN",     'tickets-train');
//        define("TICKETS_CAR",       'tickets-car');
//        define("TICKETS_INSURANCE", 'tickets-insurance');
//
//        //Passengers configs
//        define("PASSENGERS_FLIGHT",    'passengers-flight');
//        define("PASSENGERS_CHARTER",   'passengers-charter');
//        define("PASSENGERS_HOTEL",     'passengers-hotel');
//        define("PASSENGERS_TOUR",      'passengers-tour');
//        define("PASSENGERS_INSURANCE", 'passengers-insurance');
//
//        //Date plugin configs
//        define("DATE_TOUR", 'date-tour');
//
//        //Destinations configs
//        define("AVM_FLIGHT_FROM",    'avm-flight-from');
//        define("AVM_FLIGHT_TO",      'avm-flight-to');
//        define("AVM_CHARTER_FROM",   'avm-charter-from');
//        define("AVM_CHARTER_TO",     'avm-charter-to');
//        define("AVM_HOTEL_FROM",     'avm-hotel-from');
//        define("AVM_HOTEL_TO",       'avm-hotel-to');
//        define("AVM_TOUR_FROM",      'avm-tour-from');
//        define("AVM_TOUR_TO",        'avm-tour-to');
//        define("AVM_TOUR_RESORT",    'avm-tour-resort');
//        define("AVM_TRAIN_FROM",     'avm-train-from');
//        define("AVM_TRAIN_TO",       'avm-train-to');
//        define("AVM_CAR_FROM",       'avm-car-from');
//        define("AVM_CAR_TO",         'avm-car-to');
//        define("AVM_INSURANCE_FROM", 'avm-insurance-from');
//        define("AVM_INSURANCE_TO",   'avm-insurance-to');
//
//        define("AVM_RESORTS_TOUR",   'avm-resorts-tour');
//
//        define("DESTINATIONS_FLIGHT_FROM",    'Destinations_flight');
//        define("DESTINATIONS_FLIGHT_TO",      'Destinations_flight');
//        define("DESTINATIONS_CHARTER_FROM",   'Destinations_charter');
//        define("DESTINATIONS_CHARTER_TO",     'Destinations_charter');
//        define("DESTINATIONS_HOTEL_FROM",     'Destinations_hotel');
//        define("DESTINATIONS_HOTEL_TO",       'Destinations_hotel');
//        define("DESTINATIONS_TOUR_FROM",      'Destinations_tour_from');
//        define("DESTINATIONS_TOUR_TO",        'Destinations_tour_to');
//        define("DESTINATIONS_TRAIN_FROM",     'Destinations_train');
//        define("DESTINATIONS_TRAIN_TO",       'Destinations_train');
//        define("DESTINATIONS_CAR_FROM",       'Destinations_car');
//        define("DESTINATIONS_CAR_TO",         'Destinations_car');
//        define("DESTINATIONS_INSURANCE_FROM", 'Destinations_insurance');
//        define("DESTINATIONS_INSURANCE_TO",   'Destinations_insurance');
//
//        define("RESORTS_TOUR",     'Resorts_tour');

        return true;
    }
}
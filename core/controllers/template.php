<?php
namespace core\controllers;

class template
{
    private static $template;

    public static function getTemplateType($template)
    {
        if(strpos($template, MODULE) !== false) {
            $templatePath = MODULES_PATH;
        } else {
            $templatePath = PLUGINS_PATH;
        }
        return $templatePath;
    }

    public static function loadTemplate($templateFolder,$data = null,$template='index',$method = 'display')
    {
        if (!isset(self::$template)) {
            $smarty = new \smarty();
            $smarty->setCompileDir(CMP_CACHE_TMP_PATH);
            $smarty->setCacheDir(CMP_CACHE_TMP_PATH);
            self::$template = $smarty;
        }
        if($data != null) {
            foreach($data as $key =>$value) {
                self::$template->assign($key,$value);
            }
        }
        return self::$template->$method(self::getTemplateType($templateFolder).$templateFolder.TEMPLATES.$template.TPL);
    }
}
<?php
namespace core\plugins\plugin_css;

use core\controllers\app;
use core\controllers\config;

class css
{
    protected static $styleMin = true;

    public static function getStyles($styleFiles)
    {
        $style = "/*style---".time()."*/";

        foreach($styleFiles as $file) {
            $style .= file_get_contents($file);
        }

        return self:: $styleMin ? cssMin::getInstance()->run($style) : $style;
    }
}
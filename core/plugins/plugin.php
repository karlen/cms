<?php
namespace core\plugins;

use core\traits\export;
use core\traits\import;

class plugin
{
    use import;
    use export;

//    protected
//        $title,
//        $data = null,
//        $template='index',
//        $method='fetch',
//        $json='index';
//
//    public static $instance;
//
//    public static function getInstance()
//    {
//        $child_class =  get_called_class();
//        if (!self::$instance || self::$instance != $child_class) {
//            self::$instance = new $child_class();
//        }
//        return self::$instance;
//    }
//
//    protected function __construct()
//    {
//        $this->title = get_called_class();
//        $this->data['redact'] = page::getInstance()->redact;
//    }
//
//    protected function getPluginSettings($json=false)
//    {
//        $jsonFile = (!$json) ? $this->json.JSON : $json.JSON;
//        return json_decode(file_get_contents(PLUGINS_PATH.$this->title.SETTINGS_PATH.$jsonFile),true);
//    }
//
//    protected function getJson($json=false,$path=false)
//    {
//        $jsonFile = (!$json) ? $this->json.JSON : $json.JSON;
//        $jsonPath = (!$path) ? '' : $path.'/';
//        return json_decode(file_get_contents(PLUGINS_PATH.$this->title.SETTINGS_PATH.$jsonPath.$jsonFile),true);
//    }
//
//    protected function dataDecoder($data)
//    {
//        if(is_array($data)) {
//            foreach($data as $key=>$value) {
//                if(strpos($value, JSON_IDENTIFIER) !== false) {
//                    $data[$key] = json_decode($value, true);
//                } else {
//                    $data[$key] = $value;
//                }
//            }
//        } else {
//            $data = json_decode($data,true);
//        }
//        return $data;
//    }
//
//    public function getPlugin($template=false)
//    {
//        if($template) {
//            $this->template = $template;
//        }
//
//        $getDataFunction = GET_DATA_FUNC;
//
//        $this->$getDataFunction();
//
//        if(isset($this->data['template'])){
//            $this->template = $this->data['template'];
//        }
//
//        if(isset($this->data['method'])) {
//            $this->method = $this->data['method'];
//        }
//
//        return smartyTpl::loadSmarty($this->title,$this->data,$this->template,$this->method);
//    }
}
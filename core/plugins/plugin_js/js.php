<?php
namespace core\plugins\plugin_js;

use core\controllers\app;
use core\controllers\config;

class js
{
    protected static $scriptMin = true;

    public static function getScripts($scriptFiles)
    {
        $script = "/*script---".time()."*/";

        foreach($scriptFiles as $file) {
            $script .= file_get_contents($file);
        }

        return self::$scriptMin ? jsMin::run($script) : $script;
    }
}